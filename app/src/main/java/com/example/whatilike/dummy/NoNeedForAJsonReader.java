package com.example.whatilike.dummy;

import android.graphics.drawable.Drawable;

public class NoNeedForAJsonReader {

    public static final Drawable THUMB_UP = null;

    public static final Drawable THUMB_DOWN = null;

    public static final String ITEMS = "[\n" +
            "  {\n" +
            "    \"id\" : 1,\n" +
            "    \"name\" : \"Chinese LN (Wuxia)\",\n" +
            "    \"wikipediaLink\" : \"https://en.wikipedia.org/wiki/Wuxia\",\n" +
            "    \"details\" : \"Even though light novels originate from Japan, I found the wuxia style extremely satisfying, as it represents humanity transcending its mortal boundaries.\",\n" +
            "    \"likeIt\" : true\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\" : 2,\n" +
            "    \"name\" : \"Video games\",\n" +
            "    \"wikipediaLink\" : \"https://en.wikipedia.org/wiki/Video_game\",\n" +
            "    \"details\" : \"In a society where the world may seem grim to look at for some people, video games present an opportunity of escape in a world more fair to everyone.\",\n" +
            "    \"likeIt\" : true\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\" : 3,\n" +
            "    \"name\" : \"RENU\",\n" +
            "    \"wikipediaLink\" : \"https://fr.wikipedia.org/wiki/R%C3%A9volution_num%C3%A9rique\",\n" +
            "    \"details\" : \"This class given by Mr Patrelle was not interesting, as most of its content was already common knowledge for us.\",\n" +
            "    \"likeIt\" : false\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\" : 4,\n" +
            "    \"name\" : \"Donald Duck\",\n" +
            "    \"wikipediaLink\" : \"https://en.wikipedia.org/wiki/Donald_Duck\",\n" +
            "    \"details\" : \"It's a duck in a children cartoon, he's a nice guy... duck I mean!\",\n" +
            "    \"likeIt\" : true\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\" : 5,\n" +
            "    \"name\" : \"French fries\",\n" +
            "    \"wikipediaLink\" : \"https://en.wikipedia.org/wiki/French_fries\",\n" +
            "    \"details\" : \"It's good. Especially in belgium.\",\n" +
            "    \"likeIt\" : true\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\" : 6,\n" +
            "    \"name\" : \"Anime\",\n" +
            "    \"wikipediaLink\" : \"https://en.wikipedia.org/wiki/Anime\",\n" +
            "    \"details\" : \"I love both manga and anime, I find it super-nice to watch, more than a movie or a TV-series.\",\n" +
            "    \"likeIt\" : true\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\" : 7,\n" +
            "    \"name\" : \"Alcohol\",\n" +
            "    \"wikipediaLink\" : \"https://en.wikipedia.org/wiki/Alcohol\",\n" +
            "    \"details\" : \"I actually prefer water and fruit juices over alcohol. Just a matter of taste I guess.\",\n" +
            "    \"likeIt\" : false\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\" : 8,\n" +
            "    \"name\" : \"EPITA\",\n" +
            "    \"wikipediaLink\" : \"https://en.wikipedia.org/wiki/%C3%89cole_pour_l%27informatique_et_les_techniques_avanc%C3%A9es\",\n" +
            "    \"details\" : \"It's  guess I have to put it to gain some of the points I won't have anywhere anyway :'(\",\n" +
            "    \"likeIt\" : true\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\" : 9,\n" +
            "    \"name\" : \"Memes\",\n" +
            "    \"wikipediaLink\" : \"https://en.wikipedia.org/wiki/Internet_meme\",\n" +
            "    \"details\" : \"I don't always create memes, but when I do, I prefer Dos Ekkis\",\n" +
            "    \"likeIt\" : true\n" +
            "  }\n" +
            "]";
}
