package com.example.whatilike.dummy;

import android.app.Application;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 */
public class ItemContent {

    private static final Gson gson = new Gson();

    Application mApp;
    /**
     * An array of sample (dummy) items.
     */
    public static final List<ItemILikeOrNot> ITEMS = new ArrayList<>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, ItemILikeOrNot> ITEM_MAP = new HashMap<>();

    static {
        List<ItemILikeOrNot> local_items = loadItems("/res/items/items.json");
        for (ItemILikeOrNot item : local_items) {
            addItem(item);
        }
    }

    private static void addItem(ItemILikeOrNot item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static ItemILikeOrNot parseItemFromJSON(String json) {
        //TODO : implement
        return null;
    }

    private static List<ItemILikeOrNot> loadItems(String path) {
        ItemILikeOrNot[] myItemsAsArray = gson.fromJson(NoNeedForAJsonReader.ITEMS, ItemILikeOrNot[].class);
        List<ItemILikeOrNot> myItems = new ArrayList<>(Arrays.asList(myItemsAsArray));
        return myItems;
    }

    /**
     * An item I like or not
     */
    public static class ItemILikeOrNot {
        //public final Image image;
        public final String id;
        public final String name;
        public final String wikipediaLink;
        public final String details;
        public final boolean likeIt;

        public ItemILikeOrNot(String id, String name, String details, String wikipediaLink, boolean likeIt) {
            //this.image = image;
            this.id = id;
            this.name = name;
            this.details = details;
            this.wikipediaLink = wikipediaLink;
            this.likeIt = likeIt;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
